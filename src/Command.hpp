#pragma once

#include <cstddef>
#include <cstdint>
#include <memory>
#include <string>

class Command {
protected:
    uint16_t id_;

public:
    explicit Command(uint16_t);
    Command(const Command&) = default;
    Command(Command&&) = default;
    virtual uint16_t crc() const = 0;
    virtual std::string str() const = 0;
    uint16_t id() const;
    void id(uint16_t);
    virtual ~Command();
};

class CommandReader {
protected:
    uint16_t code_;

public:
    virtual size_t consume(void* data, size_t len) = 0;
    virtual size_t expect() = 0;
    virtual std::shared_ptr<Command> get() = 0;
    explicit CommandReader(uint16_t code);
    virtual ~CommandReader();

    static std::shared_ptr<CommandReader> getReader(uint16_t code);
};

class CommandNumeric final : public Command {
private:
    uint8_t value_;

public:
    explicit CommandNumeric(uint16_t id, uint8_t v);
    CommandNumeric(const CommandNumeric&) = default;
    CommandNumeric(CommandNumeric&&) = default;

    uint8_t value() const;
    void value(uint8_t);

    virtual uint16_t crc() const;
    virtual std::string str() const;
    virtual ~CommandNumeric();

    class Reader final : public CommandReader {
    private:
        enum class State;
        State state_;
        uint8_t tmp_;

    public:
        explicit Reader(uint16_t code);
        Reader(const Reader&) = delete;
        virtual size_t consume(void* data, size_t len);
        virtual size_t expect();
        virtual std::shared_ptr<Command> get();
        virtual ~Reader();
    };
};

class CommandTwoNumbers final : public Command {
private:
    uint16_t valueA_;
    uint8_t valueB_;

public:
    CommandTwoNumbers(uint16_t id, uint16_t a, uint8_t b);
    CommandTwoNumbers(const CommandTwoNumbers&) = default;
    CommandTwoNumbers(CommandTwoNumbers&&) = default;

    uint16_t valueA() const;
    void valueA(uint16_t);

    uint8_t valueB() const;
    void valueB(uint8_t);

    virtual uint16_t crc() const;
    virtual std::string str() const;
    virtual ~CommandTwoNumbers();

    class Reader final : public CommandReader {
    private:
        enum class State;
        State state_;
        uint16_t tmpA_;
        uint8_t tmpB_;

    public:
        explicit Reader(uint16_t code);
        Reader(const Reader&) = delete;
        virtual size_t consume(void* data, size_t len);
        virtual size_t expect();
        virtual std::shared_ptr<Command> get();
        virtual ~Reader();
    };
};

class CommandString final : public Command {
private:
    std::string value_;

public:
    CommandString(uint16_t id, const std::string& v);
    CommandString(const CommandString&) = default;
    CommandString(CommandString&&) = default;

    std::string& value();
    const std::string& value() const;

    void value(const std::string&);

    virtual uint16_t crc() const;
    virtual std::string str() const;
    virtual ~CommandString();

    class Reader final : public CommandReader {
    private:
        enum class State;
        State state_;
        std::string buffer_;
        size_t len_{0};

    public:
        explicit Reader(uint16_t code);
        Reader(const Reader&) = delete;
        virtual size_t consume(void* data, size_t len);
        virtual size_t expect();
        virtual std::shared_ptr<Command> get();
        virtual ~Reader();
    };
};
