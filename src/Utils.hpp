
#include <cstdint>
#include <iomanip>
#include <iostream>

namespace utils {

template <typename T>
struct _HEX {
    T val;
    size_t width;
};

template <typename T>
inline _HEX<T> hex(T val, size_t width = sizeof(T) * 2) {
    return {.val{val}, .width{width}};
}

template <typename _CharT, typename _Traits, typename T>
inline std::basic_ostream<_CharT, _Traits>&
operator<<(std::basic_ostream<_CharT, _Traits>& __os, _HEX<T> __f) {
    auto flags = __os.flags();
    auto& res = __os << std::hex << std::setw(__f.width) << std::setfill('0') << __f.val;
    res.flags(flags);
    return res;
}

uint16_t ntohs(uint16_t);
uint16_t htons(uint16_t);

} // namespace utils
