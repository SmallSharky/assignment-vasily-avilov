
#include <functional>
#include <iostream>
#include <iterator>
#include <memory>

#include <boost/asio/detail/socket_ops.hpp>
#include <boost/crc.hpp>

#include "Command.hpp"
#include "Utils.hpp"

namespace {

using CRC = boost::crc_optimal<16, 0x8005, 0, 0, true, true>;

} // namespace

Command::Command(uint16_t identifier)
    : id_(identifier) {}

Command::~Command() {}

uint16_t Command::id() const {
    return id_;
}

void Command::id(uint16_t v) {
    id_ = v;
}

CommandReader::CommandReader(uint16_t code)
    : code_(code) {}

CommandReader::~CommandReader() {}

std::shared_ptr<CommandReader> CommandReader::getReader(uint16_t code) {
    using ReaderPtr = std::shared_ptr<CommandReader>;
    using ReaderMaker = std::function<ReaderPtr(uint16_t)>;
    static const std::array readers = {
        ReaderMaker{
            [](uint16_t c) -> ReaderPtr { return std::make_shared<CommandString::Reader>(c); }},
        ReaderMaker{
            [](uint16_t c) -> ReaderPtr { return std::make_shared<CommandNumeric::Reader>(c); }},
        ReaderMaker{
            [](uint16_t c) -> ReaderPtr { return std::make_shared<CommandTwoNumbers::Reader>(c); }},

    };
    if(code && code - 1 < readers.size()) {
        return readers[code - 1](code);
    }
    return nullptr;
}

CommandNumeric::CommandNumeric(uint16_t identifier, uint8_t v)
    : Command(identifier)
    , value_(v) {}
CommandNumeric::~CommandNumeric() {}

uint8_t CommandNumeric::value() const {
    return value_;
}

void CommandNumeric::value(uint8_t v) {
    value_ = v;
}

uint16_t CommandNumeric::crc() const {
    CRC crc;
    uint16_t idNet = utils::htons(id_);
    crc.process_bytes(&idNet, sizeof(idNet));
    crc.process_bytes(&value_, sizeof(value_));
    return crc();
}
std::string CommandNumeric::str() const {
    std::stringstream ss;
    ss << "0x" << utils::hex(uint16_t(value_), 2);
    return ss.str();
}

enum class CommandNumeric::Reader::State { WAITING_DATA, DONE };

CommandNumeric::Reader::Reader(uint16_t code)
    : CommandReader(code)
    , state_(State::WAITING_DATA) {}
CommandNumeric::Reader::~Reader() {}

size_t CommandNumeric::Reader::consume(void* data, size_t len) {
    switch(state_) {
    case State::WAITING_DATA: {
        if(len < sizeof(uint8_t)) {
            return 0;
        }

        tmp_ = *static_cast<uint8_t*>(data);
        uint16_t tmp = tmp_;
        state_ = State::DONE;
        return sizeof(uint8_t);
    };
    default:
        return 0;
    };
}

size_t CommandNumeric::Reader::expect() {
    switch(state_) {
    case State::WAITING_DATA:
        return sizeof(uint8_t);
    default:
        return 0;
    }
}

std::shared_ptr<Command> CommandNumeric::Reader::get() {
    if(state_ != State::DONE) {
        return nullptr;
    }
    return std::make_shared<CommandNumeric>(code_, tmp_);
}

CommandTwoNumbers::CommandTwoNumbers(uint16_t identifier, uint16_t a, uint8_t b)
    : Command(identifier)
    , valueA_(a)
    , valueB_(b) {}
CommandTwoNumbers::~CommandTwoNumbers() {}

uint16_t CommandTwoNumbers::valueA() const {
    return valueA_;
}

void CommandTwoNumbers::valueA(uint16_t v) {
    valueA_ = v;
}

uint8_t CommandTwoNumbers::valueB() const {
    return valueB_;
}

void CommandTwoNumbers::valueB(uint8_t v) {
    valueB_ = v;
}

uint16_t CommandTwoNumbers::crc() const {
    CRC crc;
    uint16_t idNet = utils::htons(id_);
    uint16_t valueANet = utils::htons(valueA_);
    crc.process_bytes(&idNet, sizeof(idNet));
    crc.process_bytes(&valueANet, sizeof(valueANet));
    crc.process_byte(valueB_);
    return crc();
}
std::string CommandTwoNumbers::str() const {
    std::stringstream ss;
    ss << "0x" << utils::hex(valueA_) << " 0x" << utils::hex(uint16_t(valueB_), 2);
    return ss.str();
}

enum class CommandTwoNumbers::Reader::State { WAITING_DATA, DONE };

CommandTwoNumbers::Reader::Reader(uint16_t code)
    : CommandReader(code)
    , state_(State::WAITING_DATA) {}
CommandTwoNumbers::Reader::~Reader() {}

size_t CommandTwoNumbers::Reader::consume(void* data, size_t len) {
    switch(state_) {
    case State::WAITING_DATA: {
        if(len < sizeof(uint16_t) + sizeof(uint8_t)) {
            return 0;
        }
        tmpA_ = utils::ntohs(*static_cast<uint16_t*>(data));
        tmpB_ = *(static_cast<uint8_t*>(data) + sizeof(uint16_t));
        state_ = State::DONE;
        return sizeof(uint16_t) + sizeof(uint8_t);
    };
    default:
        return 0;
    };
}

size_t CommandTwoNumbers::Reader::expect() {
    switch(state_) {
    case State::WAITING_DATA:
        return sizeof(uint16_t) + sizeof(uint8_t);
    default:
        return 0;
    }
}

std::shared_ptr<Command> CommandTwoNumbers::Reader::get() {
    if(state_ != State::DONE) {
        return nullptr;
    }
    return std::make_shared<CommandTwoNumbers>(code_, tmpA_, tmpB_);
}

CommandString::CommandString(uint16_t identifier, const std::string& v)
    : Command(identifier)
    , value_(v) {}

CommandString::~CommandString() {}

const std::string& CommandString::value() const {
    return value_;
}

std::string& CommandString::value() {
    return value_;
}

void CommandString::value(const std::string& v) {
    value_ = v;
}

uint16_t CommandString::crc() const {
    CRC crc;
    uint16_t idNet = utils::htons(id_);
    crc.process_bytes(&idNet, sizeof(idNet));
    crc.process_byte(value_.size());
    crc.process_bytes(value_.data(), value_.size());
    return crc();
}

std::string CommandString::str() const {
    return value_;
}

enum class CommandString::Reader::State { WAITING_SIZE, WAITING_DATA, DONE };

CommandString::Reader::Reader(uint16_t code)
    : CommandReader(code)
    , state_(State::WAITING_SIZE) {}

CommandString::Reader::~Reader() {}

size_t CommandString::Reader::consume(void* data, size_t len) {
    switch(state_) {
    case State::WAITING_DATA: {
        size_t willRead = std::min(len_, len);
        auto charData = static_cast<char*>(data);
        std::copy(charData, charData + willRead, std::back_inserter(buffer_));
        len_ -= willRead;
        if(!len_) {
            state_ = State::DONE;
        }
        return willRead;
    };
    case State::WAITING_SIZE: {
        if(!len) {
            return 0;
        }
        len_ = *static_cast<uint8_t*>(data);
        state_ = State::WAITING_DATA;
        buffer_.reserve(len_);
        return 1;
    };
    default:
        return 0;
    };
}

size_t CommandString::Reader::expect() {
    switch(state_) {
    case State::WAITING_SIZE:
        return 1;
    case State::WAITING_DATA:
        return len_;
    default:
        return 0;
    }
}

std::shared_ptr<Command> CommandString::Reader::get() {
    if(state_ != State::DONE) {
        return nullptr;
    }
    return std::make_shared<CommandString>(code_, buffer_);
}
