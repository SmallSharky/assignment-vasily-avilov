#include <boost/asio/detail/socket_ops.hpp>

#include "Utils.hpp"

namespace utils {

uint16_t ntohs(uint16_t val) {
    return boost::asio::detail::socket_ops::network_to_host_short(val);
}

uint16_t htons(uint16_t val) {
    return boost::asio::detail::socket_ops::host_to_network_short(val);
}

} // namespace utils
