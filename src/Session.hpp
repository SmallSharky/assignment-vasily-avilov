#pragma once

#include <functional>
#include <memory>

namespace boost {
namespace asio {
template <typename A, typename B>
class basic_stream_socket;
class any_io_executor;
namespace ip {
class tcp;
}
} // namespace asio
} // namespace boost

class CommandReader;
class Command;

class Session final {
private:
    struct Impl_;
    std::unique_ptr<Impl_> impl_;
    bool err_{false};

public:
    using CommandReaderProvider = std::function<std::shared_ptr<CommandReader>(uint8_t)>;
    using CommandHandler = std::function<void(Command&)>;
    using Finally = std::function<void()>;
    using Socket =
        boost::asio::basic_stream_socket<boost::asio::ip::tcp, boost::asio::any_io_executor>;

    void expectCommand(CommandReaderProvider provider, CommandHandler handler, Finally f);
    bool isOpen();

    Session(Socket s);
    ~Session();
};
