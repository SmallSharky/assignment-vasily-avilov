#include <algorithm>
#include <cctype>
#include <iostream>

#include <boost/asio/detail/socket_ops.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/read_until.hpp>

#include "Command.hpp"
#include "Session.hpp"
#include "Utils.hpp"

namespace {

using VoidCallback = std::function<void()>;
using BoolCallback = std::function<void(bool)>;

template <size_t len>
class Buffer {
private:
    std::array<uint8_t, len> data;
    size_t data_available_{0};

public:
    void pop_front(size_t n) {
        n = std::min(data_available_, n);
        std::rotate(data.begin(), data.begin() + n, data.begin() + data_available_);
        data_available_ -= n;
    }

    void push_back(size_t n) {
        data_available_ += n;
    }

    size_t head_size() const {
        return data_available_;
    }

    size_t tail_size() const {
        return len - data_available_;
    }

    void* head() {
        return data.data();
    }

    void* tail() {
        return data.data() + data_available_;
    }
}; // class Buffer

class SocketWrapper final {
private:
    Session::Socket socket;
    bool err{false};

public:
    SocketWrapper(Session::Socket s)
        : socket(std::move(s)) {}

    template <class Buffer>
    void fetchMore(Buffer& where, VoidCallback onSuccess, VoidCallback onError) {
        if(!good()) {
            onError();
            return;
        }
        auto dataHandler{[=, &where](auto& e, size_t size) {
            if(e) {
                this->err = true;
                onError();
            } else {
                where.push_back(size);
                onSuccess();
            }
        }};
        socket.async_read_some(boost::asio::buffer(where.tail(), where.tail_size()), dataHandler);
    }

    bool good() {
        return !err;
    }
};

} // namespace

struct Session::Impl_ {
    SocketWrapper conn;
    Buffer<1024> buffer;

    Impl_(const Impl_&) = delete;
};

Session::Session(Socket s)
    : impl_(new Impl_{.conn{std::move(s)}}) {}

Session::~Session() {}

namespace {

template <class Buffer>
void consumeCommand(
    Buffer& buffer,
    std::shared_ptr<Command> cmd,
    SocketWrapper& sock,
    Session::CommandHandler onCommand,
    VoidCallback finally
) {
    if(cmd) {
        if(buffer.head_size() < sizeof(uint16_t)) {
            sock.fetchMore(
                buffer,
                [=, &buffer, &sock]() { consumeCommand(buffer, cmd, sock, onCommand, finally); },
                finally
            );
            return;
        } else {
            uint16_t crc = utils::ntohs(*static_cast<uint16_t*>(buffer.head()));
            buffer.pop_front(sizeof(uint16_t));
            if(crc == cmd->crc()) {
                onCommand(*cmd);
            } else {
                std::cout << "CRC mismatch, got " << utils::hex(crc) << ", expected "
                          << utils::hex(cmd->crc()) << std::endl;
            }
        }
    }
    finally();
}

template <class Buffer>
void readCommand(
    Buffer& buffer,
    std::shared_ptr<CommandReader> reader,
    SocketWrapper& sock,
    Session::CommandHandler onCommand,
    VoidCallback finally
) {
    if(reader) {
        size_t consumed;
        while((consumed = reader->consume(buffer.head(), buffer.head_size()))) {
            buffer.pop_front(consumed);
        }
        auto expected = reader->expect();
        if(expected) {
            sock.fetchMore(
                buffer,
                [=, &buffer, &sock]() { readCommand(buffer, reader, sock, onCommand, finally); },
                finally
            );
        } else {
            auto cmd = reader->get();
            consumeCommand(buffer, cmd, sock, onCommand, finally);
        }
    } else {
        finally();
    }
}

template <class Buffer>
void negotiateCommand(
    Buffer& buffer,
    SocketWrapper& socket,
    Session::CommandReaderProvider provider,
    Session::CommandHandler handler,
    VoidCallback finally
) {
    if(buffer.head_size() >= sizeof(uint16_t)) {
        uint16_t cmdCode = utils::ntohs(*static_cast<uint16_t*>(buffer.head()));
        auto reader = provider(cmdCode);
        buffer.pop_front(sizeof(uint16_t));
        if(reader) {
            readCommand(buffer, reader, socket, handler, finally);
        } else {
            std::cout << "Reader not negotiated for " << utils::hex(cmdCode) << std::endl;
            finally();
        }
    } else {
        socket.fetchMore(
            buffer, [&]() { negotiateCommand(buffer, socket, provider, handler, finally); }, finally
        );
    }
}

template <class Buffer>
void consumeMarker(
    Buffer& buffer,
    SocketWrapper& socket,
    Session::CommandReaderProvider provider,
    Session::CommandHandler handler,
    VoidCallback finally
) {
    auto begin = static_cast<uint8_t*>(buffer.head());
    auto end = begin + buffer.head_size();
    std::array<uint8_t, 3> win{0, 0, 0};
    auto pos = std::find_if(begin, end, [&](uint8_t val) {
        std::rotate(win.begin(), win.begin() + 1, win.end());
        win[2] = val;
        return win == std::array<uint8_t, 3>{'C', 'M', 'D'};
    });
    if(pos == end) {
        if(socket.good()) {
            buffer.pop_front((buffer.head_size() > 2 ? buffer.head_size() - 2 : 0));
        } else {
            buffer.pop_front(buffer.head_size());
        }

        socket.fetchMore(
            buffer,
            [=, &buffer, &socket]() { consumeMarker(buffer, socket, provider, handler, finally); },
            finally
        );
    } else {
        buffer.pop_front(pos - begin + 1);
        negotiateCommand(buffer, socket, provider, handler, finally);
    }
}
} // namespace

void Session::expectCommand(
    CommandReaderProvider provider, CommandHandler handler, Finally finally
) {
    consumeMarker(impl_->buffer, impl_->conn, provider, handler, finally);
}

bool Session::isOpen() {
    return impl_->conn.good() || impl_->buffer.head_size();
}
