#include <memory>

#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/tcp.hpp>

#include "Server.hpp"
#include "Session.hpp"

using tcp = boost::asio::ip::tcp;

struct Server::Impl_ {
    boost::asio::ip::tcp::acceptor acceptor;
};

Server::Server(Context& ctx, uint16_t port, SessionHandler handler)
    : impl_(new Impl_{.acceptor{ctx, tcp::endpoint(tcp::v4(), port)}})
    , handler_(handler) {
    handleSession_();
}

Server::~Server() {}

void Server::handleSession_() {
    impl_->acceptor.async_accept([this](boost::system::error_code ec, tcp::socket socket) {
        if(!ec) {
            this->handler_(std::shared_ptr<Session>(new Session(std::move(socket))));
        }
        this->handleSession_();
    });
}
