#include <iomanip>
#include <iostream>

#include <boost/asio/io_context.hpp>
#include <string>

#include "Command.hpp"
#include "Server.hpp"
#include "Session.hpp"
#include "Utils.hpp"

int main(int argc, char** argv) {
    if (argc!=2) {
        std::cout<<"Usage: "<<argv[0]<<" <port>"<<std::endl;
        return -1;
    }
    uint16_t port = std::stoul(argv[1]);
    try {
        boost::asio::io_context ctx;
        Server::SessionHandler sessionHandler{[&](std::shared_ptr<Session> session) {
            Session::CommandHandler commandHandler{[](Command& cmd) {
                std::cout << "0x" << utils::hex(cmd.id()) << ' ' << cmd.str() << std::endl;
            }};

            if(session && session->isOpen()) {
                session->expectCommand(&CommandReader::getReader, commandHandler, [=]() mutable {
                    sessionHandler(session);
                });
            }
        }};
        Server s{ctx, port, sessionHandler};
        ctx.run();
    } catch(std::exception& e) {
        std::cout << "E: " << e.what() << std::endl;
    }

    return 0;
}
