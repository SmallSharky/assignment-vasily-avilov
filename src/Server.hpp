#pragma once

#include <cstdint>
#include <functional>
#include <memory>

namespace boost {
namespace asio {
class io_context;
} // namespace asio
} // namespace boost

class Session;

class Server final {
public:
    using Context = boost::asio::io_context;
    using SessionPtr = std::shared_ptr<Session>;
    using SessionHandler = std::function<void(SessionPtr)>;

private:
    struct Impl_;

    std::unique_ptr<Impl_> impl_;
    SessionHandler handler_;

    void handleSession_();

public:
    Server(Context& ctx, uint16_t port, SessionHandler handler);
    ~Server();
};
